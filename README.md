# 2mp3-320kbps

Simple bash script for converting other formats music to mp3 320kbps files. (Note that the resolution must be higher)

## Requirements
- ffmpeg

## License
GPL-V3

## Developers

- David E. Perez Negron A.K.A P1r0
