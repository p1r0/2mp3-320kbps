#!/bin/bash
read -p "First, give me the files con conver format:" format
find . -iname '*.'$format -exec ffmpeg -i '{}' -vn -ar 44100 -ac 2 -b:a 320k '{}'.mp3 \;
#remove spaces in filenames
#rename "s/\s+//g" *.$format.ogg
#remove the old file format
rename 's/'$format'.mp3$/mp3/' *.$format.mp3
for file in *.mp3
do
  newName=`echo "$file" | sed "s/.$format//"`
  mv "$file" "$newName"
done

read -p "Do you want to remove the old files?(y|n):" option
case "$option" in
  y | Y) rm -rf *."$format"
        echo "Done!"
        ;;
  n | N) echo "OK"
        ;;
  *) echo "Unknown Option";; 
esac
